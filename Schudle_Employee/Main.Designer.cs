﻿namespace Schudle_Employee
{
    partial class Main
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.زمانبندیکارمندانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.کارمندانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.افزودنToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.لیستکارمندانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.بخشToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تقویمToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.LastVacation = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.notification = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.L_Emp = new System.Windows.Forms.Label();
            this.Main_Panel = new System.Windows.Forms.Panel();
            this.Menu_Panel = new System.Windows.Forms.Panel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.کارمندانToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.افزودنافرادToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.لیستکارمندانToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.بخشToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.تقویمToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ایجادتاریختعطیلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مشاهدهیزمانبندیToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.خانهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Main_Panel.SuspendLayout();
            this.Menu_Panel.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // زمانبندیکارمندانToolStripMenuItem
            // 
            this.زمانبندیکارمندانToolStripMenuItem.Checked = true;
            this.زمانبندیکارمندانToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.زمانبندیکارمندانToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.کارمندانToolStripMenuItem,
            this.بخشToolStripMenuItem,
            this.تقویمToolStripMenuItem});
            this.زمانبندیکارمندانToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Red;
            this.زمانبندیکارمندانToolStripMenuItem.Name = "زمانبندیکارمندانToolStripMenuItem";
            this.زمانبندیکارمندانToolStripMenuItem.Size = new System.Drawing.Size(103, 19);
            this.زمانبندیکارمندانToolStripMenuItem.Text = "زمان بندی کارمندان";
            // 
            // کارمندانToolStripMenuItem
            // 
            this.کارمندانToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.افزودنToolStripMenuItem,
            this.لیستکارمندانToolStripMenuItem});
            this.کارمندانToolStripMenuItem.Name = "کارمندانToolStripMenuItem";
            this.کارمندانToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.کارمندانToolStripMenuItem.Text = "کارمندان";
            // 
            // افزودنToolStripMenuItem
            // 
            this.افزودنToolStripMenuItem.Name = "افزودنToolStripMenuItem";
            this.افزودنToolStripMenuItem.ShowShortcutKeys = false;
            this.افزودنToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.افزودنToolStripMenuItem.Text = "افزودن";
            // 
            // لیستکارمندانToolStripMenuItem
            // 
            this.لیستکارمندانToolStripMenuItem.Name = "لیستکارمندانToolStripMenuItem";
            this.لیستکارمندانToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.لیستکارمندانToolStripMenuItem.Text = "لیست کارمندان";
            // 
            // بخشToolStripMenuItem
            // 
            this.بخشToolStripMenuItem.Name = "بخشToolStripMenuItem";
            this.بخشToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.بخشToolStripMenuItem.Text = "بخش";
            // 
            // تقویمToolStripMenuItem
            // 
            this.تقویمToolStripMenuItem.Name = "تقویمToolStripMenuItem";
            this.تقویمToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.تقویمToolStripMenuItem.Text = "تقویم";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(431, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "تاریخ";
            // 
            // LastVacation
            // 
            this.LastVacation.AutoSize = true;
            this.LastVacation.Location = new System.Drawing.Point(83, 179);
            this.LastVacation.Name = "LastVacation";
            this.LastVacation.Size = new System.Drawing.Size(35, 13);
            this.LastVacation.TabIndex = 9;
            this.LastVacation.Text = "label3";
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Location = new System.Drawing.Point(83, 43);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(35, 13);
            this.Date.TabIndex = 5;
            this.Date.Text = "label2";
            // 
            // notification
            // 
            this.notification.AutoSize = true;
            this.notification.Location = new System.Drawing.Point(321, 179);
            this.notification.Name = "notification";
            this.notification.Size = new System.Drawing.Size(142, 13);
            this.notification.TabIndex = 8;
            this.notification.Text = "آخرین تاریخ برنامه ریزی شده";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(424, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "نگهبان";
            // 
            // L_Emp
            // 
            this.L_Emp.AutoSize = true;
            this.L_Emp.Location = new System.Drawing.Point(83, 106);
            this.L_Emp.Name = "L_Emp";
            this.L_Emp.Size = new System.Drawing.Size(35, 13);
            this.L_Emp.TabIndex = 7;
            this.L_Emp.Text = "label3";
            // 
            // Main_Panel
            // 
            this.Main_Panel.Controls.Add(this.label1);
            this.Main_Panel.Controls.Add(this.LastVacation);
            this.Main_Panel.Controls.Add(this.Date);
            this.Main_Panel.Controls.Add(this.notification);
            this.Main_Panel.Controls.Add(this.label2);
            this.Main_Panel.Controls.Add(this.L_Emp);
            this.Main_Panel.Location = new System.Drawing.Point(7, 51);
            this.Main_Panel.Name = "Main_Panel";
            this.Main_Panel.Size = new System.Drawing.Size(594, 337);
            this.Main_Panel.TabIndex = 11;
            this.Main_Panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Main_Panel_Paint);
            // 
            // Menu_Panel
            // 
            this.Menu_Panel.Controls.Add(this.menuStrip2);
            this.Menu_Panel.Location = new System.Drawing.Point(6, 2);
            this.Menu_Panel.Name = "Menu_Panel";
            this.Menu_Panel.Size = new System.Drawing.Size(595, 30);
            this.Menu_Panel.TabIndex = 12;
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.کارمندانToolStripMenuItem1,
            this.بخشToolStripMenuItem1,
            this.تقویمToolStripMenuItem1,
            this.خانهToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(595, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip2_ItemClicked);
            // 
            // کارمندانToolStripMenuItem1
            // 
            this.کارمندانToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.افزودنافرادToolStripMenuItem,
            this.لیستکارمندانToolStripMenuItem1});
            this.کارمندانToolStripMenuItem1.Name = "کارمندانToolStripMenuItem1";
            this.کارمندانToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.کارمندانToolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.کارمندانToolStripMenuItem1.Text = "کارمندان";
            // 
            // افزودنافرادToolStripMenuItem
            // 
            this.افزودنافرادToolStripMenuItem.Name = "افزودنافرادToolStripMenuItem";
            this.افزودنافرادToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.افزودنافرادToolStripMenuItem.Text = "افزودن کارمند";
            this.افزودنافرادToolStripMenuItem.Click += new System.EventHandler(this.افزودنافرادToolStripMenuItem_Click);
            // 
            // لیستکارمندانToolStripMenuItem1
            // 
            this.لیستکارمندانToolStripMenuItem1.Name = "لیستکارمندانToolStripMenuItem1";
            this.لیستکارمندانToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.لیستکارمندانToolStripMenuItem1.Text = "لیست کارمندان";
            this.لیستکارمندانToolStripMenuItem1.Click += new System.EventHandler(this.لیستکارمندانToolStripMenuItem1_Click);
            // 
            // بخشToolStripMenuItem1
            // 
            this.بخشToolStripMenuItem1.Name = "بخشToolStripMenuItem1";
            this.بخشToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.بخشToolStripMenuItem1.Size = new System.Drawing.Size(39, 20);
            this.بخشToolStripMenuItem1.Text = "بخش";
            this.بخشToolStripMenuItem1.Click += new System.EventHandler(this.بخشToolStripMenuItem1_Click);
            // 
            // تقویمToolStripMenuItem1
            // 
            this.تقویمToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ایجادتاریختعطیلToolStripMenuItem,
            this.مشاهدهیزمانبندیToolStripMenuItem});
            this.تقویمToolStripMenuItem1.Name = "تقویمToolStripMenuItem1";
            this.تقویمToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.تقویمToolStripMenuItem1.Size = new System.Drawing.Size(42, 20);
            this.تقویمToolStripMenuItem1.Text = "تقویم";
            this.تقویمToolStripMenuItem1.Click += new System.EventHandler(this.تقویمToolStripMenuItem1_Click);
            // 
            // ایجادتاریختعطیلToolStripMenuItem
            // 
            this.ایجادتاریختعطیلToolStripMenuItem.Name = "ایجادتاریختعطیلToolStripMenuItem";
            this.ایجادتاریختعطیلToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.ایجادتاریختعطیلToolStripMenuItem.Text = "ایجاد تاریخ تعطیل";
            this.ایجادتاریختعطیلToolStripMenuItem.Click += new System.EventHandler(this.ایجادتاریختعطیلToolStripMenuItem_Click);
            // 
            // مشاهدهیزمانبندیToolStripMenuItem
            // 
            this.مشاهدهیزمانبندیToolStripMenuItem.Name = "مشاهدهیزمانبندیToolStripMenuItem";
            this.مشاهدهیزمانبندیToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.مشاهدهیزمانبندیToolStripMenuItem.Text = "مشاهده ی زمان بندی";
            this.مشاهدهیزمانبندیToolStripMenuItem.Click += new System.EventHandler(this.مشاهدهیزمانبندیToolStripMenuItem_Click);
            // 
            // خانهToolStripMenuItem
            // 
            this.خانهToolStripMenuItem.Name = "خانهToolStripMenuItem";
            this.خانهToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.خانهToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.خانهToolStripMenuItem.Text = "خانه";
            this.خانهToolStripMenuItem.Click += new System.EventHandler(this.خانهToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 400);
            this.Controls.Add(this.Menu_Panel);
            this.Controls.Add(this.Main_Panel);
            this.MainMenuStrip = this.menuStrip2;
            this.Name = "Main";
            this.Text = "خانه";
            this.TopMost = true;
            this.Main_Panel.ResumeLayout(false);
            this.Main_Panel.PerformLayout();
            this.Menu_Panel.ResumeLayout(false);
            this.Menu_Panel.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label notification;
        private System.Windows.Forms.Label L_Emp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LastVacation;
        private System.Windows.Forms.Panel Main_Panel;
        private System.Windows.Forms.ToolStripMenuItem زمانبندیکارمندانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem کارمندانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem افزودنToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem لیستکارمندانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem بخشToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تقویمToolStripMenuItem;
        private System.Windows.Forms.Panel Menu_Panel;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem کارمندانToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem افزودنافرادToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem لیستکارمندانToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem بخشToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem تقویمToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ایجادتاریختعطیلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مشاهدهیزمانبندیToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem خانهToolStripMenuItem;
  
    }
}