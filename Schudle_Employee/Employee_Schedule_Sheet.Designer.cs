﻿using System.Data.SqlClient;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Persia;
namespace Schudle_Employee
{
    partial class Employee_Schedule_Sheet
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void LoadFromDBDates() 
        {
            try
            {
                Turn = new Turning();

                DB db = new DB();

                DateTime EndDate = db.GetTopVactionDates();           //Get the last vacation's date

                DateTime dt= db.GetTopDates();
                dt=dt.AddDays(1);
                DateTime DATE = new DateTime(1972, 12, 3, 0, 0, 0, 0);
                
                if (dt < DATE)
                    dt = DateTime.Now;

                FillGridViewDates(dt, EndDate);             //Fill Gridview

                SetColor();
            }
            catch (System.IndexOutOfRangeException e) { MessageBox.Show("هیچ روز تعطیلی در سیستم وجود ندارد."); }  
        }

        public void FillGridViewDates(DateTime startDate, DateTime endDate )
        {
          
            endDate = endDate.AddDays(1);
            for (DateTime days = startDate; days <= endDate; days=days.AddDays(1))
            {
                Employee_Properties ep = null;
                
                DB db = new DB();

                Boolean ch = CheckDateVacation(days);

                if (!ch)
                {
                    ep = Turn.GetUnUsedEID();
                    db.InsertDateEmployee(ep.EmployeeID());
                }
                else
                {
                    ep = Turn.GetUnusedEIDVacation();
                    db.InsertVacationEmployee(ep.EmployeeID());
                 //   db.InsertDateEmployee(ep.EmployeeID());
                }
                db.RegisterDate(ep.EmployeeID(), days);
                    DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                    row.Cells[0].Value = ConvertToPersian(days);
                    row.Cells[1].Value =ep.GetLastName()+"   "+ ep.GetName();
                    dataGridView1.Rows.Add(row);
          }
        }

        private Boolean CheckDateVacation(DateTime startDate)
        {
            Boolean ch = false;
            DB db = new DB();
            if (db.CheckVacation(startDate))
                ch = true;

           
            return ch;
        }
        
        private void SetColor()
        {
            for (int i = 0; i < dataGridView1.RowCount-1; i++)
            {
                DB db = new DB();

                String date = dataGridView1.Rows[i].Cells[0].Value.ToString();

                ExactDate exd = new ExactDate(date);
              
                DateTime ext = Persia.Calendar.ConvertToGregorian(exd.GetYear(),exd.GetMounth(), exd.GetDay(), DateType.Persian);

                if (db.CheckVacation(ext))
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                else
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
            }
        }
        
        public String ConvertToPersian(DateTime Date)
        {
            Persia.SunDate sunDate = Persia.Calendar.ConvertToPersian(Date);

            String Year   =sunDate.ArrayType[0].ToString();
            String Mounth = sunDate.ArrayType[1].ToString();
            String Day    = sunDate.ArrayType[2].ToString();

            String ConvertedDate = Year + "/" + Mounth + "/" + Day;

            return ConvertedDate;
        }

       
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employee;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Button print;
        private Turning Turn;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn Dates;
        private DataGridViewTextBoxColumn Employees;
        private Button button1;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private Panel ES_Panel;
    }
}