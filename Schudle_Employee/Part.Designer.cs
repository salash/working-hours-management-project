﻿using System.Data.SqlClient;
using System.Collections.Generic;
using System;
namespace Schudle_Employee
{
    partial class Part
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        public void LoadFromDB()
        {
            Co_PartName.Items.Clear();
            DB db = new DB();
           
            LinkedList<String> List = new LinkedList<String>();
            List = db.LoadParts();

            while (List.Count!=0)
            {
                Co_PartName.Items.Add(List.First.Value.ToString());
                List.RemoveFirst();
            }

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tx_Part = new System.Windows.Forms.TextBox();
            this.L_Part = new System.Windows.Forms.Label();
            this.B_Submit = new System.Windows.Forms.Button();
            this.Co_PartName = new System.Windows.Forms.ComboBox();
            this.L1_Part = new System.Windows.Forms.Label();
            this.GB_Status = new System.Windows.Forms.GroupBox();
            this.RB_DeletePart = new System.Windows.Forms.RadioButton();
            this.RB_AddPart = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.GB_Status.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tx_Part
            // 
            this.Tx_Part.Location = new System.Drawing.Point(19, 136);
            this.Tx_Part.Name = "Tx_Part";
            this.Tx_Part.Size = new System.Drawing.Size(196, 20);
            this.Tx_Part.TabIndex = 0;
            this.Tx_Part.Visible = false;
            // 
            // L_Part
            // 
            this.L_Part.AutoSize = true;
            this.L_Part.Location = new System.Drawing.Point(497, 136);
            this.L_Part.Name = "L_Part";
            this.L_Part.Size = new System.Drawing.Size(43, 13);
            this.L_Part.TabIndex = 1;
            this.L_Part.Text = "نام بخش";
            this.L_Part.Visible = false;
            // 
            // B_Submit
            // 
            this.B_Submit.Location = new System.Drawing.Point(143, 287);
            this.B_Submit.Name = "B_Submit";
            this.B_Submit.Size = new System.Drawing.Size(290, 30);
            this.B_Submit.TabIndex = 2;
            this.B_Submit.Text = "تایید";
            this.B_Submit.UseVisualStyleBackColor = true;
            this.B_Submit.Click += new System.EventHandler(this.B_Submit_Click);
            // 
            // Co_PartName
            // 
            this.Co_PartName.FormattingEnabled = true;
            this.Co_PartName.Location = new System.Drawing.Point(19, 188);
            this.Co_PartName.Name = "Co_PartName";
            this.Co_PartName.Size = new System.Drawing.Size(196, 21);
            this.Co_PartName.TabIndex = 3;
            this.Co_PartName.Text = "بخش مورد نظر را انتخاب نمایید.";
            // 
            // L1_Part
            // 
            this.L1_Part.AutoSize = true;
            this.L1_Part.Location = new System.Drawing.Point(497, 188);
            this.L1_Part.Name = "L1_Part";
            this.L1_Part.Size = new System.Drawing.Size(43, 13);
            this.L1_Part.TabIndex = 4;
            this.L1_Part.Text = "نام بخش";
            // 
            // GB_Status
            // 
            this.GB_Status.Controls.Add(this.RB_DeletePart);
            this.GB_Status.Controls.Add(this.RB_AddPart);
            this.GB_Status.Location = new System.Drawing.Point(19, 31);
            this.GB_Status.Name = "GB_Status";
            this.GB_Status.Size = new System.Drawing.Size(530, 63);
            this.GB_Status.TabIndex = 5;
            this.GB_Status.TabStop = false;
            this.GB_Status.Text = "وضعیت";
            // 
            // RB_DeletePart
            // 
            this.RB_DeletePart.AutoSize = true;
            this.RB_DeletePart.Location = new System.Drawing.Point(43, 37);
            this.RB_DeletePart.Name = "RB_DeletePart";
            this.RB_DeletePart.Size = new System.Drawing.Size(69, 17);
            this.RB_DeletePart.TabIndex = 1;
            this.RB_DeletePart.TabStop = true;
            this.RB_DeletePart.Text = "حذف بخش";
            this.RB_DeletePart.UseVisualStyleBackColor = true;
            this.RB_DeletePart.CheckedChanged += new System.EventHandler(this.RB_DeletePart_CheckedChanged);
            // 
            // RB_AddPart
            // 
            this.RB_AddPart.AutoSize = true;
            this.RB_AddPart.Location = new System.Drawing.Point(417, 37);
            this.RB_AddPart.Name = "RB_AddPart";
            this.RB_AddPart.Size = new System.Drawing.Size(80, 17);
            this.RB_AddPart.TabIndex = 0;
            this.RB_AddPart.TabStop = true;
            this.RB_AddPart.Text = "افزودن بخش";
            this.RB_AddPart.UseVisualStyleBackColor = true;
            this.RB_AddPart.CheckedChanged += new System.EventHandler(this.RB_AddPart_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B_Submit);
            this.panel1.Controls.Add(this.GB_Status);
            this.panel1.Controls.Add(this.Co_PartName);
            this.panel1.Controls.Add(this.Tx_Part);
            this.panel1.Controls.Add(this.L_Part);
            this.panel1.Controls.Add(this.L1_Part);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 347);
            this.panel1.TabIndex = 6;
            // 
            // Part
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.panel1);
            this.Name = "Part";
            this.Text = "بخش ها";
            this.GB_Status.ResumeLayout(false);
            this.GB_Status.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox Tx_Part;
        private System.Windows.Forms.Label L_Part;
        private System.Windows.Forms.Button B_Submit;
        private System.Windows.Forms.ComboBox Co_PartName;
        private System.Windows.Forms.Label L1_Part;
        private System.Windows.Forms.GroupBox GB_Status;
        private System.Windows.Forms.RadioButton RB_DeletePart;
        private System.Windows.Forms.RadioButton RB_AddPart;
        private System.Windows.Forms.Panel panel1;

    }
}