﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Schudle_Employee
{
    public partial class Part : Form
    {
        public Part()
        {
            InitializeComponent();
            LoadFromDB();
        }

        public void LoadPanel(Panel P)
        {


            P.Controls.Add(this.B_Submit);
            P.Controls.Add(this.GB_Status);
            P.Controls.Add(this.Co_PartName);
            P.Controls.Add(this.Tx_Part);
            P.Controls.Add(this.L_Part);
            P.Controls.Add(this.L1_Part);
            P.Location = new System.Drawing.Point(12, 12);
            P.Name = "panel1";
            P.Size = new System.Drawing.Size(560, 347);
            P.TabIndex = 6;
        }

        

        private void RB_AddPart_CheckedChanged(object sender, EventArgs e)
        {
            if (RB_AddPart.Checked)
            {

                Tx_Part.Visible = true;
                L_Part.Visible = true;



                Co_PartName.Visible = false;
                L1_Part.Visible = false;

            }
        }

        private void RB_DeletePart_CheckedChanged(object sender, EventArgs e)
        {
            if (RB_DeletePart.Checked)
            {

                Tx_Part.Visible = false;
                L_Part.Visible = false;


                Co_PartName.Visible = true;
                L1_Part.Visible = true;

                LoadFromDB();
            }
        }

        private void B_Submit_Click(object sender, EventArgs e)
        {

            DB db = new DB();
            if (RB_AddPart.Checked)
            {
                db.AddPart(Tx_Part.Text);
                Co_PartName.Items.Clear();

                LoadFromDB();

                MessageBox.Show("بخش مورد نظر افزوده گردید.");
            }
            if (RB_DeletePart.Checked)
            {
                try
                {
                    db.DeletePart(Co_PartName.SelectedItem.ToString());

                    LoadFromDB();

                    MessageBox.Show("بخش مورد نظر حذف گردید.");
                }
                catch (System.NullReferenceException e2)
                {
                    MessageBox.Show("ایتدا بخش مورد نظر را برای حذف کردن انتخاب کنید.");
                }
                catch (System.Data.SqlClient.SqlException e3)
                {

                      MessageBox.Show("ابتدا کاربران بخش مورد نظر را حذف نمایید.");
                
                }
                }
        

        }

    }
}