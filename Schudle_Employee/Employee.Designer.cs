﻿using System.Data.SqlClient;
using System.Collections.Generic;
using System;
namespace Schudle_Employee
{
    partial class Employee
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code








        private void InitializeComponent()
        {
            this.Tx_Name = new System.Windows.Forms.TextBox();
            this.Tx_LastName = new System.Windows.Forms.TextBox();
            this.L_Lastname = new System.Windows.Forms.Label();
            this.Co_Part = new System.Windows.Forms.ComboBox();
            this.L_Part = new System.Windows.Forms.Label();
            this.Bu_Submit = new System.Windows.Forms.Button();
            this.Tx_NID = new System.Windows.Forms.TextBox();
            this.L_NID = new System.Windows.Forms.Label();
            this.L_Name = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Employee_P = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.Employee_P.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tx_Name
            // 
            this.Tx_Name.Location = new System.Drawing.Point(194, 25);
            this.Tx_Name.Name = "Tx_Name";
            this.Tx_Name.Size = new System.Drawing.Size(159, 20);
            this.Tx_Name.TabIndex = 1;
            // 
            // Tx_LastName
            // 
            this.Tx_LastName.Location = new System.Drawing.Point(195, 76);
            this.Tx_LastName.Name = "Tx_LastName";
            this.Tx_LastName.Size = new System.Drawing.Size(160, 20);
            this.Tx_LastName.TabIndex = 2;
            // 
            // L_Lastname
            // 
            this.L_Lastname.AutoSize = true;
            this.L_Lastname.Location = new System.Drawing.Point(474, 83);
            this.L_Lastname.Name = "L_Lastname";
            this.L_Lastname.Size = new System.Drawing.Size(69, 13);
            this.L_Lastname.TabIndex = 3;
            this.L_Lastname.Text = "نام خانوادگی";
            // 
            // Co_Part
            // 
            this.Co_Part.FormattingEnabled = true;
            this.Co_Part.Location = new System.Drawing.Point(194, 202);
            this.Co_Part.Name = "Co_Part";
            this.Co_Part.Size = new System.Drawing.Size(160, 21);
            this.Co_Part.TabIndex = 4;
            this.Co_Part.Text = "بخش مورد نظر را انتخاب نمایید.";
            // 
            // L_Part
            // 
            this.L_Part.AutoSize = true;
            this.L_Part.Location = new System.Drawing.Point(503, 202);
            this.L_Part.Name = "L_Part";
            this.L_Part.Size = new System.Drawing.Size(27, 13);
            this.L_Part.TabIndex = 5;
            this.L_Part.Text = "بخش";
            // 
            // Bu_Submit
            // 
            this.Bu_Submit.Location = new System.Drawing.Point(170, 245);
            this.Bu_Submit.Name = "Bu_Submit";
            this.Bu_Submit.Size = new System.Drawing.Size(207, 23);
            this.Bu_Submit.TabIndex = 6;
            this.Bu_Submit.Text = "تایید";
            this.Bu_Submit.UseVisualStyleBackColor = true;
            this.Bu_Submit.Click += new System.EventHandler(this.Bu_Submit_Click);
            // 
            // Tx_NID
            // 
            this.Tx_NID.Location = new System.Drawing.Point(195, 136);
            this.Tx_NID.Name = "Tx_NID";
            this.Tx_NID.Size = new System.Drawing.Size(159, 20);
            this.Tx_NID.TabIndex = 7;
            // 
            // L_NID
            // 
            this.L_NID.AutoSize = true;
            this.L_NID.Location = new System.Drawing.Point(503, 143);
            this.L_NID.Name = "L_NID";
            this.L_NID.Size = new System.Drawing.Size(40, 13);
            this.L_NID.TabIndex = 8;
            this.L_NID.Text = "کد ملی";
            // 
            // L_Name
            // 
            this.L_Name.AutoSize = true;
            this.L_Name.Location = new System.Drawing.Point(510, 25);
            this.L_Name.Name = "L_Name";
            this.L_Name.Size = new System.Drawing.Size(20, 13);
            this.L_Name.TabIndex = 11;
            this.L_Name.Text = "نام";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Employee_P);
            this.panel1.Location = new System.Drawing.Point(4, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 350);
            this.panel1.TabIndex = 12;
            // 
            // Employee_P
            // 
            this.Employee_P.Controls.Add(this.L_Name);
            this.Employee_P.Controls.Add(this.Bu_Submit);
            this.Employee_P.Controls.Add(this.Tx_NID);
            this.Employee_P.Controls.Add(this.Co_Part);
            this.Employee_P.Controls.Add(this.L_Part);
            this.Employee_P.Controls.Add(this.L_NID);
            this.Employee_P.Controls.Add(this.Tx_Name);
            this.Employee_P.Controls.Add(this.L_Lastname);
            this.Employee_P.Controls.Add(this.Tx_LastName);
            this.Employee_P.Location = new System.Drawing.Point(7, 8);
            this.Employee_P.Name = "Employee_P";
            this.Employee_P.Size = new System.Drawing.Size(558, 339);
            this.Employee_P.TabIndex = 12;
            // 
            // Employee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.panel1);
            this.Name = "Employee";
            this.Text = "پرسنل";
            this.panel1.ResumeLayout(false);
            this.Employee_P.ResumeLayout(false);
            this.Employee_P.PerformLayout();
            this.ResumeLayout(false);

        }

        /*
        public void LoadFromDB() 
        {
            Co_NID.Items.Clear() ;

            DB db = new DB();
            LinkedList<String> NID = new LinkedList<String>();
            NID = db.GetNID();


            while (NID.Count!=0)
            {

                
                Co_NID.Items.Add(NID.First.Value.ToString());

                NID.RemoveFirst();
                
            }
        }
        */
        public void LoadFromDBPart()
        {
            Co_Part.Items.Clear();
            DB db = new DB();
            LinkedList<String> List = new LinkedList<String>();
            List = db.LoadParts();


            while (List.Count!=0)
            {

                Co_Part.Items.Add(List.First.Value.ToString());
                List.RemoveFirst();
            }


        }


        #endregion

       
        private System.Windows.Forms.TextBox Tx_Name;
        private System.Windows.Forms.TextBox Tx_LastName;
        private System.Windows.Forms.Label L_Lastname;
        private System.Windows.Forms.ComboBox Co_Part;
        private System.Windows.Forms.Label L_Part;
        private System.Windows.Forms.Button Bu_Submit;
        private System.Windows.Forms.TextBox Tx_NID;
        private System.Windows.Forms.Label L_NID;
        private System.Windows.Forms.Label L_Name;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Employee_P;
    }
}

