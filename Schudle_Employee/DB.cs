﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Schudle_Employee
{
    class DB
    {
        SqlConnection  db;
        SqlCommand     query;
        
        public DB() 
        {

           db = new   SqlConnection("Server=localhost;Database=Scheduling_Employee ;Trusted_Connection=True;");

           query = new SqlCommand();

        }

        public void OpenConnection()
        {
            db.Open();
            query.Connection = db;
        }

        public void CloseConnection() 
        {
            db.Close();
        }

        public void AddEmployee(String name,String LastName,String NID,String PartName)
        {
            int pid=0;
            pid = GetPartID(PartName);
            OpenConnection();

            query.CommandText = "insert into [Scheduling_Employee].[dbo].[Employee] (name,lastname,PartId,NID) values (@Name,@LastName,@PartId,@NID)";

            query.Parameters.AddWithValue("@Name",name);
            query.Parameters.AddWithValue("@LastName", LastName);
            query.Parameters.AddWithValue("@PartId", pid);
            query.Parameters.AddWithValue("@NID", NID);

            query.ExecuteNonQuery();

            CloseConnection();
        }

        public void AddPart(String PartName)
        {
            OpenConnection();

            query.CommandText = "insert into [Scheduling_Employee].[dbo].[Part] ([PartName] ) Values (@PartName)";

            query.Parameters.AddWithValue("@PartName", PartName);

            query.ExecuteNonQuery();

            CloseConnection();
        }
        
        public LinkedList<String> ReaderToLinkedList(SqlDataReader reader,int i) 
        {
            LinkedList<String> List = new LinkedList<string>();
            while (reader.Read())
                List.AddFirst(reader.GetString(i));

            return List;
        
        }
       
        public void DeleteEmployee(int Employee_Id)
        {

            OpenConnection();
            query.CommandText = "Delete from [Scheduling_Employee].[dbo].[Date] where Employee_Id=@Employee_key";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@Employee_key", Employee_Id);
            query.ExecuteNonQuery();
            CloseConnection();

            OpenConnection();
            query.CommandText = "Delete from [Scheduling_Employee].[dbo].[Vacation] where Employee_Id=@Employee_key1";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@Employee_key1", Employee_Id);
            query.ExecuteNonQuery();
            CloseConnection();

            
            OpenConnection();
            query.CommandText = "Delete from [Scheduling_Employee].[dbo].[Employee] where ID=@Employee_Id";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@Employee_Id", Employee_Id);
            query.ExecuteNonQuery();
            CloseConnection();
        
        
        }
       
        public void InsertDate(int id,String Date)
        {
            OpenConnection();
            query.CommandText = "Insert into [Scheduling_Employee].[dbo].[Date]   ([Employee_Id],[Date]) values(@id,@Date)";
            query.Parameters.AddWithValue("@id",id);
            query.Parameters.AddWithValue("@Date", Date);
            query.ExecuteNonQuery();
            CloseConnection();
        }

       public void InsertVacation(DateTime Date)
        {
            OpenConnection();
            query.CommandText = "Insert into [Scheduling_Employee].[dbo].[Vacation] ([Date]) values(@Date)";
            query.Parameters.AddWithValue("@Date", Date);
            query.ExecuteNonQuery();
            CloseConnection();
        }
        
        public void InsertVacationEmployee(int Employee) 
        {
            Employee_Properties ep = GetEmployeeName(Employee.ToString());
            int CoD = ep.GetCoD();
            CoD++;
            int CoV = ep.GetCoV();
            CoV++;

            OpenConnection();

            query.CommandText = "update [Scheduling_Employee].[dbo].[Employee]  set [CoD]=@COD ,[CoV]=@COV where [Id]=@EmployeeID ";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@EmployeeID", Employee);
            query.Parameters.AddWithValue("@COD", CoD);
            query.Parameters.AddWithValue("@COV", CoV);
            query.ExecuteNonQuery();
            CloseConnection();
        }

        public void InsertDateEmployee(int Employee)
        {
            Employee_Properties ep = GetEmployeeName(Employee.ToString());
            int CoD = ep.GetCoD();
            CoD++;
            
            OpenConnection();

            query.CommandText = "update [Scheduling_Employee].[dbo].[Employee]  set [CoD]=@COD where [Id]=@EmployeeID ";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@EmployeeID", Employee);
            query.Parameters.AddWithValue("@COD", CoD);
           
            query.ExecuteNonQuery();
            CloseConnection();
        }

        public void RegisterDate(int id, DateTime date)
        {
          //  int ID = Convert.ToInt32(id);

            OpenConnection();

            query.CommandText = "insert into [Scheduling_Employee].[dbo].[Date] ([Employee_Id],[Date]) values (@id,@date) ";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@id", id);
            query.Parameters.AddWithValue("@date", date);

            query.ExecuteNonQuery();
            CloseConnection();
        
        }
        
        public Boolean CheckVacation(DateTime Date) 
        {
            OpenConnection();
            query.CommandText = "select count([id]) from [Scheduling_Employee].[dbo].[Vacation] where  [Date]=@VD  and [Enable]=1 ";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@VD",Date.Date);

            Object ob=null;
               ob = query.ExecuteScalar();
            CloseConnection();
           
            if (Convert.ToInt32(ob) ==0)
                return false;
            else
                return true; 
        }
       
        public void DeleteVacation(DateTime Date)
        {
            OpenConnection();
            query.CommandText = "Delete from [Scheduling_Employee].[dbo].[Vacation] where [Date]=@Date";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@Date", Date);
            query.ExecuteNonQuery();
            CloseConnection();
        }
       
        public DateTime GetTopVactionDates()
        {
            OpenConnection();
            DateTime Da=new DateTime() ;

            query.CommandText = "select  Top 1 *  from [Scheduling_Employee].[dbo].[Vacation]  where [Enable]=1 AND Employee_Id is null order by [Date] DESC";

            SqlDataReader reader = query.ExecuteReader();

            while (reader.Read())
            {
                Da = reader.GetDateTime(2);
            }
            CloseConnection();

            return Da;
        }

        public DateTime GetTopDates()
        {
            OpenConnection();
            DateTime Da = new DateTime();

            query.CommandText = "select  Top 1 [Date] from [Scheduling_Employee].[dbo].[Date] where  Employee_Id is not null order by [Date] DESC";

            Object dt = query.ExecuteScalar();

           
            CloseConnection();

            return Convert.ToDateTime(dt);
        }

        public String GetUnUsedEmployee()
        {
            Object EID = null;
            OpenConnection();
         
            query.CommandText = "select  top 1 id from [Scheduling_Employee].[dbo].[Employee] as a where a.cod=(select MIN(COD) from [Scheduling_Employee].[dbo].[Employee])";
            EID = query.ExecuteScalar();
            CloseConnection();

            
                return EID.ToString();
        }

        public String GetUnUsedEmployeeForVacation()
        {
            OpenConnection();
            query.CommandText = "select  top 1 id from [Scheduling_Employee].[dbo].[Employee] as a where a.cov=(select MIN(COV) from [Scheduling_Employee].[dbo].[Employee]) ;";

            Object EIDV = query.ExecuteScalar();
            CloseConnection();
            if (EIDV == null)
                return null;
            else
            return EIDV.ToString();
        }


        public int GetPartID(String PartName) 
        {
            OpenConnection();
            query.CommandText = "Select id  From [Scheduling_Employee].[dbo].[Part] where PartName=@part";
            query.Parameters.AddWithValue("@part", PartName);
            Object PID=query.ExecuteScalar();
            CloseConnection();
            return Convert.ToInt32(PID);

        }
       
        public Employee_Properties GetEmployeeName(String id)
        {
            String name = null, lastname = null; 
            int EID = 0, cod=0,CoV=0;

            OpenConnection();
            query.CommandText = "SELECT    [id],[name],[lastname],[CoD],[CoV]  FROM [Scheduling_Employee].[dbo].[Employee] where [id]=@Employeekey";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@Employeekey", Convert.ToInt32(id));

            SqlDataReader reader = query.ExecuteReader();

            while (reader.Read())
            {
                name = reader.GetString(1);

                lastname = reader.GetString(2);

                EID = reader.GetInt32(0);

                cod = reader.GetInt32(3);
                
                CoV = reader.GetInt32(4);
            }
            CloseConnection();

            Employee_Properties ep = new Employee_Properties(name, lastname, EID, cod, CoV);

            return ep;
        }
        
        public LinkedList<Employee_Properties> GetEmployee()
        {
            String name = null, lastname = null, NID=null;
            int EID = 0;

            LinkedList<Employee_Properties> se = new LinkedList<Employee_Properties>();

            OpenConnection();
            query.CommandText = "SELECT    [id],[name]  ,[lastname],[NID],[CoD],[CoV]  FROM [Scheduling_Employee].[dbo].[Employee] ";
            
            SqlDataReader reader = query.ExecuteReader();

            while (reader.Read())
            {
                name = reader.GetString(1);

                lastname = reader.GetString(2);

                EID = reader.GetInt32(0);

                NID = reader.GetString(3);

                Employee_Properties ep = new Employee_Properties(name, lastname, EID, NID, reader.GetInt32(4), reader.GetInt32(5));
                
                se.AddFirst(ep);
            }
            CloseConnection();

          
            return se;

        }

        public LinkedList<RD> LoadDate(int weeknumber) 
        {

            LinkedList<RD> lrd = new LinkedList<RD>();

            OpenConnection();
            query.CommandText=@"SELECT TOP (@weeks) b.name,b.lastname ,[Date] 
                                FROM [Scheduling_Employee].[dbo].[Date] as a join [Scheduling_Employee].[dbo].[Employee] as b on a.[Employee_Id]=b.id
                                order by a.[Date] ASC";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@weeks", (weeknumber*7));

            SqlDataReader reader = query.ExecuteReader();

            while (reader.Read())
            {
               String  name = reader.GetString(0);

               String  lastname = reader.GetString(1);

               DateTime EID = reader.GetDateTime(2);

                RD rd = new RD(EID,name,lastname);

                lrd.AddFirst(rd);
            
            }

            return lrd;
        
        
        
        }

        public void DeletePart(String Part) 
        {
            OpenConnection();
            query.CommandText = "Delete From [Scheduling_Employee].[dbo].[Part] where PartName=@part";
            query.Parameters.AddWithValue("@Part",Part);
            query.ExecuteNonQuery();
            CloseConnection();
        }

        public LinkedList<String> LoadParts()
        {
            OpenConnection();
            query.CommandText = "Select PartName from [Scheduling_Employee].[dbo].[Part]";
            LinkedList<String> List = new LinkedList<String>();
            List=ReaderToLinkedList( query.ExecuteReader(),0);
            CloseConnection();
            return List;
        }

        public String GetEmployeePart(int p)
        {
            OpenConnection();
            query.CommandText = "Select PartName from Employee join part on Employee.partId  =Part.Id where Employee.id=@id";
            query.Parameters.Clear();
            query.Parameters.AddWithValue("@id", p);

            Object PartName= query.ExecuteScalar();

            CloseConnection();

            return PartName.ToString();
        }

        public String FindEmployee(DateTime dateTime)
        {
            
            OpenConnection();

            query.CommandText = "SELECT [Employee_Id]  FROM [Scheduling_Employee].[dbo].[Date] where cast([Date] as date)=@dateTime";


            query.Parameters.Clear();

            query.Parameters.AddWithValue("@dateTime", dateTime);

            Object id=query.ExecuteScalar();


            CloseConnection();

            if (id == null)
                return "هیچ کارمندی مشخص نگردیده است.";
            else
            {

                Employee_Properties ep = GetEmployeeName(id.ToString());
                return ep.GetName()+"      "+ep.GetLastName();

            }


        }
    }
}
