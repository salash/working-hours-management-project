﻿using System.Data.SqlClient;
namespace Schudle_Employee
{
    partial class Calender
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.Bu_Submite = new System.Windows.Forms.Button();
            this.monthCalendarX1 = new BehComponents.MonthCalendarX();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edite_Date = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.Employee_Schedule = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Bu_Submite
            // 
            this.Bu_Submite.Location = new System.Drawing.Point(457, 239);
            this.Bu_Submite.Name = "Bu_Submite";
            this.Bu_Submite.Size = new System.Drawing.Size(111, 30);
            this.Bu_Submite.TabIndex = 3;
            this.Bu_Submite.Text = "افزودن";
            this.Bu_Submite.UseVisualStyleBackColor = true;
            this.Bu_Submite.Click += new System.EventHandler(this.Bu_Submite_Click);
            // 
            // monthCalendarX1
            // 
            this.monthCalendarX1.BoldedDayForeColor = System.Drawing.Color.Blue;
            this.monthCalendarX1.BorderColor = System.Drawing.Color.CadetBlue;
            this.monthCalendarX1.CalendarType = BehComponents.CalendarTypes.Persian;
            this.monthCalendarX1.DayRectTickness = 2F;
            this.monthCalendarX1.DaysBackColor = System.Drawing.Color.LightGray;
            this.monthCalendarX1.DaysFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.monthCalendarX1.DaysForeColor = System.Drawing.Color.DodgerBlue;
            this.monthCalendarX1.EnglishAnnuallyBoldedDates = new System.DateTime[0];
            this.monthCalendarX1.EnglishBoldedDates = new System.DateTime[0];
            this.monthCalendarX1.EnglishHolidayDates = new System.DateTime[0];
            this.monthCalendarX1.EnglishMonthlyBoldedDates = new System.DateTime[0];
            this.monthCalendarX1.HolidayForeColor = System.Drawing.Color.Red;
            this.monthCalendarX1.HolidayWeekly = BehComponents.MonthCalendarX.DayOfWeekForHoliday.Friday;
            this.monthCalendarX1.LineWeekColor = System.Drawing.Color.Black;
            this.monthCalendarX1.Location = new System.Drawing.Point(29, 35);
            this.monthCalendarX1.Name = "monthCalendarX1";
            this.monthCalendarX1.PersianAnnuallyBoldedDates = new BehComponents.PersianDateTime[0];
            this.monthCalendarX1.PersianBoldedDates = new BehComponents.PersianDateTime[0];
            this.monthCalendarX1.PersianHolidayDates = new BehComponents.PersianDateTime[0];
            this.monthCalendarX1.PersianMonthlyBoldedDates = new BehComponents.PersianDateTime[0];
            this.monthCalendarX1.ShowToday = true;
            this.monthCalendarX1.ShowTodayRect = true;
            this.monthCalendarX1.ShowToolTips = false;
            this.monthCalendarX1.ShowTrailing = true;
            this.monthCalendarX1.Size = new System.Drawing.Size(281, 229);
            this.monthCalendarX1.Style_DaysButton = BehComponents.ButtonX.ButtonStyles.Simple;
            this.monthCalendarX1.Style_GotoTodayButton = BehComponents.ButtonX.ButtonStyles.Green;
            this.monthCalendarX1.Style_MonthButton = BehComponents.ButtonX.ButtonStyles.Blue;
            this.monthCalendarX1.Style_NextMonthButton = BehComponents.ButtonX.ButtonStyles.Green;
            this.monthCalendarX1.Style_PreviousMonthButton = BehComponents.ButtonX.ButtonStyles.Green;
            this.monthCalendarX1.Style_YearButton = BehComponents.ButtonX.ButtonStyles.Blue;
            this.monthCalendarX1.TabIndex = 11;
            this.monthCalendarX1.TitleBackColor = System.Drawing.Color.Wheat;
            this.monthCalendarX1.TitleFont = new System.Drawing.Font("Tahoma", 8.25F);
            this.monthCalendarX1.TitleForeColor = System.Drawing.Color.Black;
            this.monthCalendarX1.TodayBackColor = System.Drawing.Color.Wheat;
            this.monthCalendarX1.TodayFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.monthCalendarX1.TodayForeColor = System.Drawing.Color.Black;
            this.monthCalendarX1.TodayRectColor = System.Drawing.Color.Coral;
            this.monthCalendarX1.TodayRectTickness = 2F;
            this.monthCalendarX1.TrailingForeColor = System.Drawing.Color.DarkGray;
            this.monthCalendarX1.WeekDaysBackColor = System.Drawing.Color.Wheat;
            this.monthCalendarX1.WeekDaysFont = new System.Drawing.Font("Tahoma", 8.25F);
            this.monthCalendarX1.WeekDaysForeColor = System.Drawing.Color.OrangeRed;
            this.monthCalendarX1.WeekStartsOn = BehComponents.MonthCalendarX.WeekDays.Saturday;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Edite_Date});
            this.dataGridView1.Location = new System.Drawing.Point(328, 35);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 198);
            this.dataGridView1.TabIndex = 12;
            // 
            // Date
            // 
            this.Date.HeaderText = "تاریخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Edite_Date
            // 
            this.Edite_Date.HeaderText = "حذف";
            this.Edite_Date.Name = "Edite_Date";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(328, 239);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 30);
            this.button1.TabIndex = 13;
            this.button1.Text = "حذف";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Employee_Schedule
            // 
            this.Employee_Schedule.Location = new System.Drawing.Point(332, 284);
            this.Employee_Schedule.Name = "Employee_Schedule";
            this.Employee_Schedule.Size = new System.Drawing.Size(236, 28);
            this.Employee_Schedule.TabIndex = 14;
            this.Employee_Schedule.Text = "زمان بندی افراد";
            this.Employee_Schedule.UseVisualStyleBackColor = true;
            this.Employee_Schedule.Click += new System.EventHandler(this.Employee_Schedule_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Employee_Schedule);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.monthCalendarX1);
            this.panel1.Controls.Add(this.Bu_Submite);
            this.panel1.Location = new System.Drawing.Point(17, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 342);
            this.panel1.TabIndex = 15;
            // 
            // Calender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 362);
            this.Controls.Add(this.panel1);
            this.Name = "Calender";
            this.Text = "تقویم";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        public void LoadFromDB()
        {
            /*
            DB db = new DB();
            db.OpenConnection();

            SqlDataReader reader;
            reader = db.GetNID();


            while (reader.Read())
            {

                string nid = reader.GetString(0);

                CO_NID.Items.Add(nid);

                //reader.NextResult();

            }
            db.CloseConnection();*/
        }

        #endregion


        private System.Windows.Forms.Button Bu_Submite;
        private BehComponents.MonthCalendarX monthCalendarX1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Edite_Date;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Employee_Schedule;
        private System.Windows.Forms.Panel panel1;

    }
}