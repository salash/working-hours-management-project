﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schudle_Employee
{
    public class Employee_Properties
    {
        private String Name;
        private String LastName;
        private int ID;
        private String NID;
        private int CoD;
        private int CoV;

        public int GetCoD()
        {
        
          return CoD;
        }

        public int GetCoV()
        {

            return CoV;
        }

        public Employee_Properties(String Name, String LastName, int ID, int CoD, int CoV) 
        {
            this.Name = Name;
            this.LastName = LastName;
            this.ID =ID;

            this.CoD = CoD;
            this.CoV = CoV;
           
        }

        public Employee_Properties(String Name, String LastName, int ID, String NID, int CoD, int CoV)
        {
            this.Name = Name;
            this.LastName = LastName;
            this.ID = ID;
            this.NID = NID;
            this.CoD = CoD;
            this.CoV = CoV;
        }


        public String GetNID() { return this.NID; }

        public String GetName() 
        {
            return this.Name;
        }

        public String GetLastName() 
        {
            return this.LastName;
        }

        public int EmployeeID()
        {
            return this.ID;
        
        }
        
    }

}
