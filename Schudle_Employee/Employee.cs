﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Schudle_Employee
{
    public partial class Employee : Form
    {
        public Employee()
        {
            InitializeComponent();
            LoadFromDBPart();
        }


     


        private void Bu_Submit_Click(object sender, EventArgs e)
        {

            DB db = new DB();
     
                try
                {

                    db.AddEmployee(Tx_Name.Text, Tx_LastName.Text, Tx_NID.Text, Co_Part.SelectedItem.ToString());

                

                    MessageBox.Show("پرسنل مورد نظر افزوده گردید.");
                }
                catch (System.NullReferenceException e4)
                {

                    MessageBox.Show("اطلاعات کاربر مورد نظر را به صورت کامل وارد نمایید.");
                }
            }


        public void LoadPanel(Panel P) 
        {
            P.Controls.Add(this.L_Name);
            P.Controls.Add(this.Bu_Submit);
            P.Controls.Add(this.Tx_NID);
            P.Controls.Add(this.Co_Part);
            P.Controls.Add(this.L_Part);
            P.Controls.Add(this.L_NID);
            P.Controls.Add(this.Tx_Name);
            P.Controls.Add(this.L_Lastname);
            P.Controls.Add(this.Tx_LastName);
            P.Location = new System.Drawing.Point(7, 8);
            P.Name = "Employee_P";
            P.Size = new System.Drawing.Size(558, 339);
            P.TabIndex = 12;

        }
    }
}