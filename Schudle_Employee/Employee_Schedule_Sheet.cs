﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Persia;

namespace Schudle_Employee
{
    public partial class Employee_Schedule_Sheet : Form
    {
        public Employee_Schedule_Sheet()
        {
            InitializeComponent();
          
            LoadFromDBDates();
           
        }

        
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Dates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Employees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            this.ES_Panel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.ES_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Dates,
            this.Employees});
            this.dataGridView1.Location = new System.Drawing.Point(39, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 266);
            this.dataGridView1.TabIndex = 0;
            // 
            // Dates
            // 
            this.Dates.HeaderText = "تاریخ";
            this.Dates.Name = "Dates";
            this.Dates.ReadOnly = true;
            // 
            // Employees
            // 
            this.Employees.HeaderText = "کارمند";
            this.Employees.Name = "Employees";
            this.Employees.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 329);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "چاپ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // printDocument2
            // 
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument2_PrintPage);
            // 
            // ES_Panel
            // 
            this.ES_Panel.Controls.Add(this.button1);
            this.ES_Panel.Controls.Add(this.dataGridView1);
            this.ES_Panel.Location = new System.Drawing.Point(2, 3);
            this.ES_Panel.Name = "ES_Panel";
            this.ES_Panel.Size = new System.Drawing.Size(563, 356);
            this.ES_Panel.TabIndex = 2;
            // 
            // Employee_Schedule_Sheet
            // 
            this.ClientSize = new System.Drawing.Size(564, 355);
            this.Controls.Add(this.ES_Panel);
            this.Name = "Employee_Schedule_Sheet";
            this.Load += new System.EventHandler(this.Employee_Schedule_Sheet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ES_Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void Employee_Schedule_Sheet_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            printDocument2.Print();
        

        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(bm, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
            e.Graphics.DrawImage(bm, 0, 0);
        }
   
        public Panel LoadPanel(Panel P)
        {
            P.Controls.Add(this.button1);
            P.Controls.Add(this.dataGridView1);
            P.Location = new System.Drawing.Point(2, 3);
            P.Name = "ES_Panel";
            P.Size = new System.Drawing.Size(563, 356);
            P.TabIndex = 2;

            return P;
        }

    }
}
