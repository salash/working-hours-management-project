﻿namespace Schudle_Employee
{
    partial class AllEmployees
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Del_but = new System.Windows.Forms.Button();
            this.print = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.AllEmployee_P = new System.Windows.Forms.Panel();
            this.chx = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Part = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.AllEmployee_P.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chx,
            this.Name,
            this.LastName,
            this.NID,
            this.Part,
            this.id});
            this.dataGridView1.Location = new System.Drawing.Point(3, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(540, 221);
            this.dataGridView1.TabIndex = 0;
            // 
            // Del_but
            // 
            this.Del_but.Location = new System.Drawing.Point(3, 273);
            this.Del_but.Name = "Del_but";
            this.Del_but.Size = new System.Drawing.Size(223, 30);
            this.Del_but.TabIndex = 1;
            this.Del_but.Text = "حذف";
            this.Del_but.UseVisualStyleBackColor = true;
            this.Del_but.Click += new System.EventHandler(this.Del_but_Click);
            // 
            // print
            // 
            this.print.Location = new System.Drawing.Point(368, 273);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(186, 29);
            this.print.TabIndex = 2;
            this.print.Text = "چاپ";
            this.print.UseVisualStyleBackColor = true;
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // AllEmployee_P
            // 
            this.AllEmployee_P.Controls.Add(this.print);
            this.AllEmployee_P.Controls.Add(this.dataGridView1);
            this.AllEmployee_P.Controls.Add(this.Del_but);
            this.AllEmployee_P.Location = new System.Drawing.Point(12, 12);
            this.AllEmployee_P.Name = "AllEmployee_P";
            this.AllEmployee_P.Size = new System.Drawing.Size(587, 368);
            this.AllEmployee_P.TabIndex = 3;
            // 
            // chx
            // 
            this.chx.HeaderText = "انتخاب";
            this.chx.Name = "chx";
            // 
            // Name
            // 
            this.Name.HeaderText = "نام";
            this.Name.Name = "Name";
            this.Name.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.HeaderText = "نام خانوادگی";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // NID
            // 
            this.NID.HeaderText = "کد ملی";
            this.NID.Name = "NID";
            this.NID.ReadOnly = true;
            // 
            // Part
            // 
            this.Part.HeaderText = "بخش";
            this.Part.Name = "Part";
            this.Part.ReadOnly = true;
            // 
            // id
            // 
            this.id.HeaderText = "Key";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // AllEmployees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.AllEmployee_P);
          //  this.Name = "AllEmployees";
            this.Text = "کارمندان";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.AllEmployee_P.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Del_but;
        private System.Windows.Forms.Button print;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Panel AllEmployee_P;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn NID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Part;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
    }
}