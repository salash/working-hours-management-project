﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Schudle_Employee
{
    public partial class AllEmployees : Form
    {
        public AllEmployees()
        {
            InitializeComponent();

            LoadFromDBEmployee();
        }

        private void LoadFromDBEmployee()
        {
            LinkedList<Employee_Properties> ep = new LinkedList<Employee_Properties>();
            DB db = new DB();
            ep=db.GetEmployee();

            while (  ep.Count>0)
            {
                Employee_Properties emp =ep.First.Value ;
                String Part = db.GetEmployeePart(emp.EmployeeID());
                dataGridView1.Rows.Add(false,emp.GetName(), emp.GetLastName(), emp.GetNID(),Part,emp.EmployeeID());
                ep.RemoveFirst();
            
            
            }



        }

        private void Del_but_Click(object sender, EventArgs e)
        {
            
            DB db = new DB();


            for (int i = dataGridView1.Rows.Count - 1; i >= 0; i--)
            {
                if ((bool)dataGridView1.Rows[i].Cells[0].FormattedValue)
                {
                    

                    String EID=dataGridView1.Rows[i].Cells[5].FormattedValue.ToString();
                
                    db.DeleteEmployee(Convert.ToInt32( EID));

                    dataGridView1.Rows.RemoveAt(i);

                }
            }

        }

        private void print_Click(object sender, EventArgs e)
        {
            printDocument1.Print();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(bm, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
            e.Graphics.DrawImage(bm, 0, 0);
        }

        public void LoadPanel(Panel P) 
        {

            P.Controls.Add(this.print);
            P.Controls.Add(this.dataGridView1);
            P.Controls.Add(this.Del_but);
            P.Location = new System.Drawing.Point(34, 12);
            P.Name = "AllEmployee_P";
            P.Size = new System.Drawing.Size(554, 368);
            P.TabIndex = 3;
        
        
        
        
        
        
        
        
        }
    
    
    }
}
