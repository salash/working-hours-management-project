﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schudle_Employee
{
    class Turning
    {

        private Employee_Properties EID;
        private DB db;
       
        public Turning() 
        {
            db = new DB();
            EID = null;
            
        }

        public Employee_Properties  GetUnUsedEID() 
        {
              String id = null;
              id= db.GetUnUsedEmployee();

              EID= db.GetEmployeeName(id);
            
              return EID; 
        }

        public Employee_Properties GetUnusedEIDVacation() 
        {
            String id = null;
          
            id = db.GetUnUsedEmployeeForVacation();   
        
            EID = db.GetEmployeeName(id);
            return EID; 
        }
    }
}