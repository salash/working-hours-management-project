﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Schudle_Employee
{
    public partial class ReviewPosts : Form
    {
        public ReviewPosts()
        {
            int n = 1;
            InitializeComponent();
            LoadDates(n);
            LoadWeekNumber();
        
        }

        public void LoadWeekNumber() 
        {
           for(int i=1;i<=8;i++)
               Co_Week.Items.Add(i);
        }



        public void LoadPanel(Panel P)
        {

            P.Controls.Add(this.dataGridView1);
            P.Location = new System.Drawing.Point(2, 1);
            P.Name = "panel1";
            P.Size = new System.Drawing.Size(597, 345);
            P.TabIndex = 0;

            P.Controls.Add(this.Co_Week);
            P.Controls.Add(this.label1);

        }

        public void LoadDates(int WeekNumber) 
        {
            dataGridView1.Rows.Clear();

            DB db = new DB();

            LinkedList<RD> lrd = new LinkedList<RD>();

            lrd = db.LoadDate(WeekNumber);

           while (lrd.Count > 0)
           {
               RD rd = lrd.First();

               String ln=rd.lastname;
               String name=rd.name;
               String Name = name + "   " + ln;



               DateTime dt= rd.date;
               Persia.SunDate a = Persia.Calendar.ConvertToPersian(dt);
               String dates = a.Persian.ToString();

               DataGridViewRow dr = new DataGridViewRow();
               DataGridViewCell date = new DataGridViewTextBoxCell();

               DataGridViewCell E = new DataGridViewTextBoxCell();

               date.Value = dates;
               E.Value = Name;

               dr.Cells.Add(date);
               dr.Cells.Add(E);
               if(CheckDateVacation(dt))
                   dr.DefaultCellStyle.BackColor = Color.Green; 
               else
                   dr.DefaultCellStyle.BackColor = Color.White; 
               dataGridView1.Rows.Add(dr);

               lrd.RemoveFirst();
                      
           }
         
       }

        private Boolean CheckDateVacation(DateTime startDate)
        {
            Boolean ch = false;
            DB db = new DB();
            if (db.CheckVacation(startDate))
                ch = true;


            return ch;
        }

        private void Co_Week_SelectedIndexChanged(object sender, EventArgs e)
        {

            String s = Co_Week.SelectedItem.ToString();

            LoadDates(Convert.ToInt32(s)) ;



        }

    }
}
