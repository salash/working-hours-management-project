﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Persia;


namespace Schudle_Employee
{
    public partial class Calender : Form
    {
        public static string TodayDate;

        public Calender()
        {
            InitializeComponent();

            LoadFromDB();

           // Employee_Schedule.Visible = false;
            Check();
        }


        public void Check() 
        {
            DB db = new DB();
           DateTime dt= db.GetTopVactionDates();
           Boolean  ch = CheckDateRegistered(dt);
           if(!ch)
               Employee_Schedule.Visible = false;
           else

               Employee_Schedule.Visible = true;


        
        
        }

        public void LoadPanel(Panel P)
        {

            P.Controls.Add(this.Employee_Schedule);
            P.Controls.Add(this.dataGridView1);
            P.Controls.Add(this.button1);
            P.Controls.Add(this.monthCalendarX1);
            P.Controls.Add(this.Bu_Submite);
            P.Location = new System.Drawing.Point(17, 12);
            P.Name = "panel1";
            P.Size = new System.Drawing.Size(586, 342);
            P.TabIndex = 15;
        
        
        
        
        
        
        
        
        }


        public String DateTimetoString(DateTime date)
        {
            String dates = null;
            try
            {
                Persia.SunDate a = Persia.Calendar.ConvertToPersian(date);

                dates = a.Persian.ToString();
            }
            catch (Exception e) { }
            return dates;
        }


        public Boolean CheckDateRegistered(DateTime dt)
        {

            DB db = new DB();
            DateTime dts = db.GetTopDates();
            if (dt <= dts)
                return false;
            else
                return true;
        
        
        
        }

        public Boolean CheckPastDate(DateTime dt)
        {
            DateTime DATE = DateTime.Now;
            DATE=DATE.AddDays(-1);
            if (dt < DATE)
                return false;
            else
                return true;
        
        }
      
        private void Bu_Submite_Click(object sender, EventArgs e)
        {
                    String Dates = null;
                    DB db = new DB();
                   DateTime dts= db.GetTopDates();
                   Dates=DateTimetoString(dts);
                    
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////           
                   String  Date     = monthCalendarX1.GetSelectedDateInPersianDateTime().ToShortDateString();
               
                    ExactDate dt = new ExactDate(Date);
               
                    DateTime ext = Persia.Calendar.ConvertToGregorian(dt.GetYear(), dt.GetMounth(), dt.GetDay(), DateType.Persian);

                    if (CheckPastDate(ext))
                    {
                        Boolean Existe = CheckDateRegistered(ext);
                        if (Existe)
                        {
                            Boolean ch = CheckDateExist(ext);
                            if (!ch)
                            {
                                dataGridView1.Rows.Add(Date);



                                db.InsertVacation(ext);

                            }
                            else
                                MessageBox.Show("این تاریخ  درج شده است");
                        }
                        else
                            MessageBox.Show("سیستم تا پایان تاریخ " + Dates + "برنامه ریزی شده است");
                    }
                    else
                        MessageBox.Show("برنامه ریزی برای روز های گذشته امکان پذیر نیست.");
                    Check();
        }

        private Boolean CheckDateExist(DateTime Date)
        {
            Boolean ch=false;
            DB db = new DB();
            ch=db.CheckVacation(Date);
            return ch;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = dataGridView1.Rows.Count - 1; i >= 0; i--)
                {
                    if ((bool)dataGridView1.Rows[i].Cells[1].FormattedValue)
                    {
                        String tx = dataGridView1.Rows[i].Cells["Date"].Value.ToString();

                        ExactDate exd = new ExactDate(tx);
                        
                        DateTime ext = Persia.Calendar.ConvertToGregorian(exd.GetYear(), exd.GetMounth(), exd.GetDay(), DateType.Persian);

                        DB db = new DB();
                        db.DeleteVacation(ext);
                        dataGridView1.Rows.RemoveAt(i);
                    }
                }
            }
            catch (System.NullReferenceException E1)
            {
                MessageBox.Show("تاریخی برای حذف وجود ندارد.");
            
            }
            Check();
        }

        private void Employee_Schedule_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            Employee_Schedule_Sheet ESS = new Employee_Schedule_Sheet();
        
           ESS.Show();
         //  button1.Visible = false;
           Check();
        }
    }
}

