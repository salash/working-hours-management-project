﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Schudle_Employee
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            LoadFromDB();
          
        }
        
        public void LoadFromDB() 
        {
            DB db = new DB();
            DateTime date=  db.GetTopVactionDates();
           LastVacation.Text = DateTimetoString(date);
/////////////////////////////////////////////////////////////////////////////////////
           Date.Text = DateTimetoString(DateTime.Now.Date);
///////////////////////////////////////////////////////////////////////////////////
          
          String s= DateTime.Now.ToString("yyyy-MM-dd");
         
          String ep = db.FindEmployee(DateTime.Today.Date);
           L_Emp.Text = ep;

        }


        public String DateTimetoString(DateTime date)
        {
            String dates = null;
            try
            {

                Persia.SunDate a = Persia.Calendar.ConvertToPersian(date);

                 dates = a.Persian.ToString();

                
            }
            catch (Exception e)
            {

                dates="تاریخی در سیستم ثبت نشده است";
            }
            return dates;
        }

        private void Main_Panel_Paint(object sender, PaintEventArgs e)
        {

        }


        private void افزودنافرادToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Employee ep = new Employee();
            Main_Panel.Controls.Clear();
            ep.LoadPanel(Main_Panel);




        }

        private void لیستکارمندانToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Main_Panel.Controls.Clear();
            AllEmployees AE = new AllEmployees();
            AE.LoadPanel(Main_Panel);



        }

        private void بخشToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Part part = new Part();
            Main_Panel.Controls.Clear();
            part.LoadPanel(Main_Panel);
        }

        private void ایجادتاریختعطیلToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Calender ca = new Calender();
            Main_Panel.Controls.Clear();
            ca.LoadPanel(Main_Panel);

        }

        private void مشاهدهیزمانبندیToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReviewPosts rp = new ReviewPosts();
            Main_Panel.Controls.Clear();
            rp.LoadPanel(Main_Panel);
        }

        private void خانهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main_Panel.Controls.Clear();
            Main m = new Main();
            m.LoadPanel(Main_Panel);
        }

        public void LoadPanel(Panel P) 
        {

             
            P.Controls.Add(this.label1);
            P.Controls.Add(this.LastVacation);
            P.Controls.Add(this.Date);
            P.Controls.Add(this.notification);
            P.Controls.Add(this.label2);
            P.Controls.Add(this.L_Emp);
            P.Location = new System.Drawing.Point(7, 51);
            P.Name = "Main_Panel";
            P.Size = new System.Drawing.Size(594, 337);
            P.TabIndex = 11;
            P.Paint += new System.Windows.Forms.PaintEventHandler(this.Main_Panel_Paint);
        
        }

        private void تقویمToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

    }
}
